package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.AdminBo;
import model.bo.KhachHangBo;

/**
 * Servlet implementation class DeleteOrderCus
 */
@WebServlet("/delete-order-cus")
public class DeleteOrderCus extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteOrderCus() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {
	    String orderId = request.getParameter("mhd");
	    if (orderId != null) {
		KhachHangBo khbo = new KhachHangBo();
		boolean deleteSuccess = khbo.huyDatHang(orderId);

		if (deleteSuccess == true) {
		    request.setAttribute("deleteSuccess", orderId);
		}
		RequestDispatcher rd = request.getRequestDispatcher("order-history");
		rd.forward(request, response);
		return;

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
