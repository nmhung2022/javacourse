package controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.AdminBo;

/**
 * Servlet implementation class UpdateOrder
 */
@WebServlet("/update-order")
public class UpdateOrder extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateOrder() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {
	    String orderId = request.getParameter("mhd");
	    String ttOrder = request.getParameter("tt");

	    if (orderId != null) {
		AdminBo adbo = new AdminBo();
		int delivery = adbo.chuyenTrangThai(orderId, ttOrder);

		if (delivery > 0) {
		    request.setAttribute("delivery", orderId);
		}
		RequestDispatcher rd = request.getRequestDispatcher("manage-order");
		rd.forward(request, response);
		return;

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
