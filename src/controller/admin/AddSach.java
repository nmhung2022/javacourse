package controller.admin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import model.bean.LoaiBean;
import model.bean.SachBean;
import model.bo.AdminBo;
import model.bo.LoaiBo;

/**
 * Servlet implementation class AddSach
 */

@MultipartConfig
@WebServlet("/add-sach")
public class AddSach extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddSach() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {
	    String maSach = request.getParameter("ms");

	    String tenSach = request.getParameter("ts");

	    String tacGia = request.getParameter("tg");
	    String maLoai = request.getParameter("ml");
	    String gia = request.getParameter("gia");

	    String anh = null;

	    Long giaBan = null;

	    if (gia != null)
		giaBan = Long.parseLong(gia);

	    String anhUpdate = request.getParameter("anhUpdated");

	    LoaiBo lbo = new LoaiBo();

	    ArrayList<LoaiBean> dsloai = lbo.getLoai();

	    request.setAttribute("dsloai", dsloai);

	    if (maSach != null && tenSach != null && tacGia != null) {
		Part part = request.getPart("anh");

		String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();
		if (!fileName.equals("")) {
//		   String realPath = "D:\\Development\\JavaProject\\JavaCourse\\WebContent\\image_sach";
		    String realPath = request.getServletContext().getRealPath("image_sach");
		    if (!Files.exists(Path.of(realPath))) {
			Files.createDirectories(Path.of(realPath));
		    }
		    part.write(realPath + "/" + fileName);
		    anh = "image_sach/" + fileName;
		} else {
		    anh = anhUpdate;
		}

		AdminBo adbo = new AdminBo();

		int isEditSuccess = adbo.kiemTraTS(maSach, tenSach, tacGia, giaBan, anh, maLoai);
		System.out.println(isEditSuccess);
		if (isEditSuccess == -1) {
		    SachBean existsMaSach = new SachBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
		    request.setAttribute("existsMaSach", existsMaSach);
		    RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-sach.jsp");
		    rd.forward(request, response);
		    return;
		} else {
		    SachBean newSach = new SachBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
		    request.setAttribute("sachNew", newSach);
		    RequestDispatcher rd = request.getRequestDispatcher("dashboard");
		    rd.forward(request, response);
		    return;
		}

	    }
	    RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-sach.jsp");
	    rd.forward(request, response);
	} catch (

	Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
