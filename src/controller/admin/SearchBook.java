package controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.SachBean;
import model.bo.LoaiBo;
import model.bo.SachBo;

/**
 * Servlet implementation class SearchBook
 */
@WebServlet("/searchbook")
public class SearchBook extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchBook() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	try {
	    request.setCharacterEncoding("UTF-8");
	    response.setCharacterEncoding("UTF-8");

	    SachBo sbo = new SachBo();

	    ArrayList<SachBean> dssach = sbo.getSach();

	    String ml = request.getParameter("ml");
	    String key = request.getParameter("txttk");

	    if (ml != null)
		dssach = sbo.timMaLoai(ml);
	    else if (key != null) {
		dssach = sbo.timChung(key);
	    }

	    request.setAttribute("dssach", dssach);
	    request.setAttribute("timkiemsach", key);

	    
	    RequestDispatcher rd = request.getRequestDispatcher("views/admin/dashboard.jsp");
	    rd.forward(request, response);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
