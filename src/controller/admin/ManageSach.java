package controller.admin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import model.bean.LoaiBean;
import model.bean.SachBean;
import model.bo.AdminBo;
import model.bo.LoaiBo;

/**
 * Servlet implementation class ManageSach
 */
@MultipartConfig
@WebServlet("/manage-sach")
public class ManageSach extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManageSach() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {

	    String maSach = request.getParameter("ms");

	    String tenSach = request.getParameter("ts");
	    
	
	    String tacGia = request.getParameter("tg");
	    String maLoai = request.getParameter("ml");
	    String gia = request.getParameter("gia");
	    String anh = request.getParameter("anh");
	    
	    Long giaBan = null;
	    if (gia != null)
		giaBan = Long.parseLong(gia);
	    String update = request.getParameter("update");

	    String anhUpdate = request.getParameter("anhUpdated");
	    
	    LoaiBo lbo = new LoaiBo();

	    ArrayList<LoaiBean> dsloai = lbo.getLoai();

	    request.setAttribute("dsloai", dsloai);

	    if (maSach != null && tenSach != null && tacGia != null) {
		if (update != null) {
		    AdminBo adbo = new AdminBo();

		    Part part = request.getPart("anh");

		    String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();

		    if (!fileName.equals("")) {
			String realPath = "D:\\Development\\JavaProject\\JavaCourse\\WebContent\\image_sach";
			if (!Files.exists(Path.of(realPath))) {
			    Files.createDirectories(Path.of(realPath));
			}
			part.write(realPath + "/" + fileName);
			anh = "image_sach/" + fileName;
		    } else {
			anh = anhUpdate;
		    }

		    boolean isEditSuccess = adbo.kiemTraCNSach(maSach, tenSach, tacGia, giaBan, anh, maLoai);
		    if (isEditSuccess == true) {
			SachBean sb = new SachBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
			request.setAttribute("sachEdit", sb);
			RequestDispatcher rd = request.getRequestDispatcher("views/admin/edit-sach.jsp");
			rd.forward(request, response);
			return;
		    }
		}
		SachBean sb = new SachBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
		request.setAttribute("sachEdit", sb);
		RequestDispatcher rd = request.getRequestDispatcher("views/admin/edit-sach.jsp");
		rd.forward(request, response);
		return;
	    }
	    RequestDispatcher rd = request.getRequestDispatcher("dashboard");
	    rd.forward(request, response);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
