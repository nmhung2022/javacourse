package model.bean;

import java.util.Date;

public class LichSuMuaHangBean {
  
    private String maHoaDon;
    private String maSach;
    private String anh;
    private String tenSach;
    private Date thoiGianMua;
    private Long tongTien;
    private int trangThai;
    
    public LichSuMuaHangBean(String maHoaDon, String maSach, String anh, String tenSach, Date thoiGianMua,
	    Long tongTien, int trangThai) {
	super();
	this.maHoaDon = maHoaDon;
	this.maSach = maSach;
	this.anh = anh;
	this.tenSach = tenSach;
	this.thoiGianMua = thoiGianMua;
	this.tongTien = tongTien;
	this.trangThai = trangThai;
    }
    public LichSuMuaHangBean() {
	super();
	// TODO Auto-generated constructor stub
    }
    public String getMaHoaDon() {
        return maHoaDon;
    }
    public void setMaHoaDon(String maHoaDon) {
        this.maHoaDon = maHoaDon;
    }
    public String getMaSach() {
        return maSach;
    }
    public void setMaSach(String maSach) {
        this.maSach = maSach;
    }
    public String getAnh() {
        return anh;
    }
    public void setAnh(String anh) {
        this.anh = anh;
    }
    public String getTenSach() {
        return tenSach;
    }
    public void setTenSach(String tenSach) {
        this.tenSach = tenSach;
    }
    public Date getThoiGianMua() {
        return thoiGianMua;
    }
    public void setThoiGianMua(Date thoiGianMua) {
        this.thoiGianMua = thoiGianMua;
    }
    public Long getTongTien() {
        return tongTien;
    }
    public void setTongTien(Long tongTien) {
        this.tongTien = tongTien;
    }
    public int getTrangThai() {
        return trangThai;
    }
    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }
    public void setThoiGianMua(String string) {
	// TODO Auto-generated method stub
	
    }
    
    
  
}
