package model.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.bean.AdminBean;
import model.bean.LichSuMuaHangBean;
import model.bean.SachBean;
import model.bo.LichSuMuaHangBo;

public class AdminDao {
    public AdminBean kiemTraDangNhap(String username, String password) throws Exception, SQLException {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Admin_Authenticate(?, ?)}");

	cs.setString(1, username);
	cs.setString(2, password);

	ResultSet rs = cs.executeQuery();

	if (rs.next() == false) {
	    return null;
	} else {
	    AdminBean admin = new AdminBean();
	    admin.setTenDangNhap(rs.getString("TenDangNhap"));
	    admin.setMatKhau(rs.getString("MatKhau"));
	    admin.setQuyen(rs.getBoolean("Quyen"));
	    return admin;
	}
    }

    public int kiemTraDangKy(String username, String password) throws Exception, SQLException {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Admin_Register(?, ?, ?)}");

	cs.setString(1, username);
	cs.setString(2, password);
	cs.registerOutParameter(3, Types.INTEGER);

	cs.execute();

	int result = cs.getInt(3);

	return result;
    }

    public boolean kiemTraCapNhatSach(String masach, String tensach, String tacgia, Long gia, String anh, String maloai)
	    throws Exception {

	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Book_Update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

	cs.setString(1, masach);
	cs.setString(2, tensach);
	cs.setLong(3, 30L);
	cs.setLong(4, gia);
	cs.setString(5, maloai);
	cs.setString(6, "10");
	cs.setString(7, anh);

	Date date = new Date();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");

	cs.setString(8, sdf.format(date));
	cs.setString(9, tacgia);

	cs.registerOutParameter(10, Types.BIT);

	cs.execute();

	boolean result = cs.getBoolean(10);

	return result;
    }

    public boolean kiemTraXoaSach(String maSach) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Book_Delete(?, ?)}");

	cs.setString(1, maSach);

	cs.registerOutParameter(2, Types.BIT);

	cs.execute();

	boolean result = cs.getBoolean(2);

	return result;
    }

    public int kiemTraThemSach(String maSach, String tenSach, String tacGia, Long giaBan, String anh, String maLoai)
	    throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Book_Insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
	cs.setString(1, maSach);
	cs.setString(2, tenSach);
	cs.setLong(3, 30L);
	cs.setLong(4, giaBan);
	cs.setString(5, maLoai);
	cs.setString(6, "10");
	cs.setString(7, anh);

	Date date = new Date();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");

	cs.setString(8, sdf.format(date));
	cs.setString(9, tacGia);

	cs.registerOutParameter(10, Types.BIGINT);

	cs.execute();

	int result = cs.getInt(10);

	return result;
    }

    public int kiemTraThemLoai(String maLoai, String tenLoai) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_TypesOfBooks_Insert(?, ?, ?)}");
	cs.setString(1, maLoai);
	cs.setString(2, tenLoai);

	cs.registerOutParameter(3, Types.BIGINT);

	cs.execute();

	int result = cs.getInt(3);

	return result;
    }

    public boolean kiemTraCapNhatLoai(String maLoai, String maLoaiOld, String tenLoai) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_TypesOfBooks_Update(?, ?, ?, ?)}");
	cs.setString(1, maLoai);
	cs.setString(2, maLoaiOld);
	cs.setString(3, tenLoai);

	cs.registerOutParameter(4, Types.BIT);

	cs.execute();

	boolean result = cs.getBoolean(4);
	return result;

    }

    public boolean kiemTraXoaLoai(String maLoai) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_TypesOfBooks_Delete(?, ?)}");

	cs.setString(1, maLoai);

	cs.registerOutParameter(2, Types.BIT);

	cs.execute();

	boolean result = cs.getBoolean(2);

	return result;
    }

    public LichSuMuaHangBo layLichSuDathang() throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Admin_GetAllOrderHistory}");

	ResultSet rs = cs.executeQuery();

	LichSuMuaHangBo list = new LichSuMuaHangBo();

	while (rs.next()) {

	    LichSuMuaHangBean ls = new LichSuMuaHangBean();

	    ls.setTenSach(rs.getString("tensach"));
	    ls.setAnh(rs.getString("anh"));
	    ls.setTongTien(rs.getLong("SoLuongMua") * rs.getLong("gia"));
	    ls.setTrangThai(rs.getInt("TrangThai"));
	    ls.setThoiGianMua(rs.getDate("NgayMua"));
	    ls.setMaSach(rs.getString("MaSach"));
	    ls.setMaHoaDon(rs.getString("MaHoaDon"));

	    list.ThemObject(ls);
	}
	return list;

    }

    public boolean xoaHoaDon(String orderId) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Delete(?, ?)}");

	cs.setString(1, orderId);

	cs.registerOutParameter(2, Types.BIT);

	cs.execute();

	boolean result = cs.getBoolean(2);

	return result;
    }

    public int chuyenTrangThaiDonHang(String orderId, String ttOrder) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Transforms(?, ?, ?)}");

	cs.setString(1, orderId);
	cs.setString(2, ttOrder);

	cs.registerOutParameter(3, Types.BIGINT);

	cs.execute();

	int result = cs.getInt(3);

	return result;
    }
}
