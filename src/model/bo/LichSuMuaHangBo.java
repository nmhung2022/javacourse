package model.bo;

import java.util.ArrayList;
import java.util.Date;


import model.bean.LichSuMuaHangBean;

public class LichSuMuaHangBo {
    public ArrayList<LichSuMuaHangBean> ds = new ArrayList<LichSuMuaHangBean>();

    public LichSuMuaHangBo() {
	super();
	// TODO Auto-generated constructor stub
    }

    public long tongTien() {
	long total = 0;
	for (int i = 0; i < ds.size(); i++) {
	    if(ds.get(i).getTrangThai() == 1) {
		total += ds.get(i).getTongTien();
	    }
	}
	return total;
    }

    public void Them(String maHoaDon, String maSach, String anh, String tenSach, Date thoiGianMua, Long tongTien,
	    int trangThai) {
//	int n = ds.size();
//	for (LichSuMuaHangBean g : ds) {
//		if (g.getMaHoaDon().toLowerCase().trim().equals(maHoaDon.toLowerCase().trim())) {
//			g.get
//			return;
//		}
//
//	}
	LichSuMuaHangBean h = new LichSuMuaHangBean(maHoaDon, maSach, anh, tenSach, thoiGianMua, tongTien, trangThai);
	ds.add(h);

    }

    public void ThemObject(LichSuMuaHangBean lsb) {
	ds.add(lsb);
    }

}
