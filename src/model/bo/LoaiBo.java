package model.bo;

import java.util.ArrayList;

import model.bean.LoaiBean;
import model.bean.SachBean;
import model.dao.LoaiDao;

public class LoaiBo {
    LoaiDao ldao = new LoaiDao();

    public ArrayList<LoaiBean> getLoai() throws Exception {
	return ldao.getLoaiSach();
    }

    public ArrayList<LoaiBean> timMaLoai(String maLoai) throws Exception {
	ArrayList<LoaiBean> ds = ldao.getLoaiSach();
	ArrayList<LoaiBean> tam = new ArrayList<LoaiBean>();
	for (LoaiBean l : ds) {
	    if (l.getTenloai().toLowerCase().contains(maLoai.trim().toLowerCase()))
		tam.add(l);
	}
	return tam;
    }
}
