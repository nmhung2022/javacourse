package model.bo;

import model.bean.AdminBean;
import model.dao.AdminDao;

public class AdminBo {
    AdminDao admin = new AdminDao();

    public AdminBean kiemTraDN(String tendn, String pass) throws Exception {
	return admin.kiemTraDangNhap(tendn, pass);
    }

    public int kiemTraDK(String tendn, String pass) throws Exception {
	return admin.kiemTraDangKy(tendn, pass);
    }

    public boolean kiemTraCNSach(String maSach, String tenSach, String tacGia, Long giaBan, String anh, String maLoai)
	    throws Exception {
	return admin.kiemTraCapNhatSach(maSach, tenSach, tacGia, giaBan, anh, maLoai);
    }

    public boolean kiemTraXS(String maSach) throws Exception {
	return admin.kiemTraXoaSach(maSach);
    }

    public int kiemTraTS(String maSach, String tenSach, String tacGia, Long giaBan, String anh, String maLoai)
	    throws Exception {
	return admin.kiemTraThemSach(maSach, tenSach, tacGia, giaBan, anh, maLoai);
    }

    public int kiemTraTL(String maLoai, String tenLoai) throws Exception {
	return admin.kiemTraThemLoai(maLoai, tenLoai);
    }

    public boolean kiemTraCNL(String maLoai, String maLoaiOld, String tenLoai) throws Exception {
	return admin.kiemTraCapNhatLoai(maLoai, maLoaiOld, tenLoai);
    }

    public boolean kiemTraXL(String maLoai) throws Exception {
	return admin.kiemTraXoaLoai(maLoai);
    }

    public LichSuMuaHangBo layLichSuDathang() throws Exception {
	return admin.layLichSuDathang();
    }

    public boolean xoaHoaDon(String orderId) throws Exception {
	return admin.xoaHoaDon(orderId);
	
    }

    public int chuyenTrangThai(String orderId, String ttOrder) throws Exception {
	return admin.chuyenTrangThaiDonHang(orderId, ttOrder);
    }

}
