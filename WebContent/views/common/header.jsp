<%@page import="model.bo.GioHangBo"%>
<%@page import="model.bean.KhachHangBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<style>
#Modal style


.modal-login {
	width: 350px;
}

.modal-login .modal-content {
	padding: 20px;
	border-radius: 5px;
	border: none;
}

.modal-login .modal-header {
	border-bottom: none;
	position: relative;
	justify-content: center;
}

.modal-login .close {
	position: absolute;
	top: -10px;
	right: -10px;
}

.modal-login h4 {
	color: #636363;
	text-align: center;
	font-size: 26px;
	margin-top: 0;
}

.modal-login .modal-content {
	color: #999;
	border-radius: 1px;
	margin-bottom: 15px;
	background: #fff;
	border: 1px solid #f3f3f3;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 25px;
}

.modal-login .form-group {
	margin-bottom: 20px;
}

.modal-login label {
	font-weight: normal;
	font-size: 13px;
}

.modal-login .form-control {
	min-height: 38px;
	padding-left: 5px;
	box-shadow: none !important;
	border-width: 0 0 1px 0;
	border-radius: 0;
}

.modal-login .form-control:focus {
	border-color: #ccc;
}

.modal-login .input-group-addon {
	max-width: 42px;
	text-align: center;
	background: none;
	border-bottom: 1px solid #ced4da;
	padding-right: 5px;
	border-radius: 0;
}

.modal-login .btn, .modal-login .btn:active {
	font-size: 16px;
	font-weight: bold;
	background: #19aa8d !important;
	border-radius: 3px;
	border: none;
	min-width: 140px;
}

.modal-login .btn:hover, .modal-login .btn:focus {
	background: #179b81 !important;
}

.modal-login .hint-text {
	text-align: center;
	padding-top: 5px;
	font-size: 13px;
}

.modal-login .modal-footer {
	color: #999;
	border-color: #dee4e7;
	text-align: center;
	margin: 0 -25px -25px;
	font-size: 13px;
	justify-content: center;
}

.modal-login a {
	color: #fff;
	text-decoration: underline;
}

.modal-login a:hover {
	text-decoration: none;
}

.modal-login a {
	color: #19aa8d;
	text-decoration: none;
}

.modal-login a:hover {
	text-decoration: underline;
}

.modal-login .fa {
	font-size: 21px;
	position: relative;
	top: 6px;
}

#message-password {
	display: none;
}
</style>

	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");

	int sizee = 0;
	if (session.getAttribute("giohang") != null) {
		GioHangBo ghboo = new GioHangBo();
		ghboo = (GioHangBo) session.getAttribute("giohang");
		sizee = ghboo.ds.size();
	}

	if (request.getAttribute("isSignin") != null && (boolean) request.getAttribute("isSignin") == false) {
	%>
	<script>
		$(document).ready(function() {
			$("#loginModal").modal('show');
		});
	</script>
	<%
	}
	%>




	<div class="container-fuild">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="home">Trang chủ</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="cart">Giỏ
							hàng <span class="badge badge-light rounded-circle"><%=sizee%></span>
							<span class="sr-only">unread messages</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="payment">Thanh
							Toán</a></li>

					<li class="nav-item"><a class="nav-link" href="order-history">Lịch
							sử mua hàng</a></li>
				</ul>

				<ul class="navbar-nav ml-auto mr-2">
					<%
					KhachHangBean khh = (KhachHangBean) session.getAttribute("auth");
					int flagAuthh;

					if (khh != null) {
					%>
					<li class="nav-item"><a class="nav-link" href="profile">Chào
							mừng <%=khh.getHoTen()%>
					</a></li>
					<form action="signout" method="POST">
						<li class="nav-item"><button
								class="nav-link btn btn-sm btn-outline-secondary" href="signin">Đăng
								xuất</button></li>
					</form>
					<%
					} else {
					if (session.getAttribute("flag_auth") == null) {
						flagAuthh = 0;
					} else {
						flagAuthh = (int) session.getAttribute("flag_auth");
					}

					if (flagAuthh == 1) {
					%>

					<script>
						$(document).ready(function() {
							$("#loginModal").modal('show');
							$("#message-password").css("display", "block");
						});
					</script>
					<%
					}
					%>
					<li class="nav-item mr-3"><a
						class="btn btn-sm btn-outline-secondary nav-link"
						data-target="#loginModal" data-toggle="modal">Đăng nhập</a></li>
					<li class="nav-item"><a
						class="btn btn-sm btn-outline-secondary nav-link" href="signup?new=<%= true %>">Đăng
							ký</a></li>
					<%
					}
					%>
				</ul>
			</div>
		</nav>


		<!-- Modal HTML -->
		<div id="loginModal" class="modal fade">
			<div class="modal-dialog modal-login">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Đăng nhập</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<form action="signin" method="post">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
									<input type="text" class="form-control" name="username"
										placeholder="Tên đăng nhập" required="required">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" class="form-control" name="password"
										placeholder="Mật khẩu" required="required">
								</div>
								<div class="alert alert-danger mt-3" role="alert"
									id="message-password">Tên đăng nhập hoặc mật khẩu không
									chính xác!</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block btn-lg">Đăng
									nhập</button>
							</div>
							<p class="hint-text">
								<a href="forgot">Quên mật khẩu?</a>
							</p>
						</form>
					</div>
					<div class="modal-footer">
						<a href="signup">Tạo mới tài khoản</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
