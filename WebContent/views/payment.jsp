
<%@page import="model.bean.KhachHangBean"%>
<%@page import="model.bo.GioHangBo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Thanh toán</title>

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>
</head>
<body>

	<style>
body {
	background: #f5f5f5
}

.rounded {
	border-radius: 1rem
}

.nav-pills .nav-link {
	color: #555
}

.nav-pills .nav-link.active {
	color: white
}

input[type="radio"] {
	margin-right: 5px
}

.bold {
	font-weight: bold
}

#Modal style

.modal-login {
	width: 350px;
}

.modal-login .modal-content {
	padding: 20px;
	border-radius: 5px;
	border: none;
}

.modal-login .modal-header {
	border-bottom: none;
	position: relative;
	justify-content: center;
}

.modal-login .close {
	position: absolute;
	top: -10px;
	right: -10px;
}

.modal-login h4 {
	color: #636363;
	text-align: center;
	font-size: 26px;
	margin-top: 0;
}

.modal-login .modal-content {
	color: #999;
	border-radius: 1px;
	margin-bottom: 15px;
	background: #fff;
	border: 1px solid #f3f3f3;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 25px;
}

.modal-login .form-group {
	margin-bottom: 20px;
}

.modal-login label {
	font-weight: normal;
	font-size: 13px;
}

.modal-login .form-control {
	min-height: 38px;
	padding-left: 5px;
	box-shadow: none !important;
	border-width: 0 0 1px 0;
	border-radius: 0;
}

.modal-login .form-control:focus {
	border-color: #ccc;
}

.modal-login .input-group-addon {
	max-width: 42px;
	text-align: center;
	background: none;
	border-bottom: 1px solid #ced4da;
	padding-right: 5px;
	border-radius: 0;
}

.modal-login .btn, .modal-login .btn:active {
	font-size: 16px;
	font-weight: bold;
	background: #19aa8d !important;
	border-radius: 3px;
	border: none;
	min-width: 140px;
}

.modal-login .btn:hover, .modal-login .btn:focus {
	background: #179b81 !important;
}

.modal-login .hint-text {
	text-align: center;
	padding-top: 5px;
	font-size: 13px;
}

.modal-login .modal-footer {
	color: #999;
	border-color: #dee4e7;
	text-align: center;
	margin: 0 -25px -25px;
	font-size: 13px;
	justify-content: center;
}

.modal-login a {
	color: #fff;
	text-decoration: underline;
}

.modal-login a:hover {
	text-decoration: none;
}

.modal-login a {
	color: #19aa8d;
	text-decoration: none;
}

.modal-login a:hover {
	text-decoration: underline;
}

.modal-login .fa {
	font-size: 21px;
	position: relative;
	top: 6px;
}

#message-password {
	display: none;
}
</style>




	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");

	int size = 0;
	if (session.getAttribute("giohang") != null) {
		GioHangBo ghbo = new GioHangBo();

		ghbo = (GioHangBo) session.getAttribute("giohang");

		size = ghbo.ds.size();
	}

	if (request.getAttribute("isSignin") != null && (boolean) request.getAttribute("isSignin") == false) {
	%>
	<script>
		$(document).ready(function() {
			$("#loginModal").modal('show');
		});
	</script>
	<%
	}
	%>




	<div class="container-fuild">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="home">Trang chủ</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="cart">Giỏ
							hàng <span class="badge badge-light rounded-circle"><%=size%></span>
							<span class="sr-only">unread messages</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="payment">Thanh
							Toán</a></li>

					<li class="nav-item"><a class="nav-link" href="order-history">Lịch
							sử mua hàng</a></li>
				</ul>

				<ul class="navbar-nav ml-auto mr-2">
					<%
					KhachHangBean kh = (KhachHangBean) session.getAttribute("auth");
					int flagAuth;

					if (kh != null) {
					%>
					<li class="nav-item"><a class="nav-link" href="profile">Chào
							mừng <%=kh.getHoTen()%>
					</a></li>
					<form action="signout" method="POST">
						<li class="nav-item"><button
								class="nav-link btn btn-sm btn-outline-secondary" href="signin">Đăng
								xuất</button></li>
					</form>
					<%
					} else {
					if (session.getAttribute("flag_auth") == null) {
						flagAuth = 0;
					} else {
						flagAuth = (int) session.getAttribute("flag_auth");
					}

					if (flagAuth == 1) {
					%>
					<script>
						$(document).ready(function() {
							$("#loginModal").modal('show');
							$("#message-password").css("display", "block");
						});
					</script>
					<%
					}
					%>
					<li class="nav-item mr-3"><a
						class="btn btn-sm btn-outline-secondary nav-link"
						data-target="#loginModal" data-toggle="modal">Đăng nhập</a></li>
					<li class="nav-item"><a
						class="btn btn-sm btn-outline-secondary nav-link" href="signup">Đăng
							ký</a></li>
					<%
					}
					%>
				</ul>
			</div>
		</nav>


		<!-- Modal HTML -->
		<div id="loginModal" class="modal fade">
			<div class="modal-dialog modal-login">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Đăng nhập</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<form action="signin" method="post">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
									<input type="text" class="form-control" name="username"
										placeholder="Tên đăng nhập" required="required">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" class="form-control" name="password"
										placeholder="Mật khẩu" required="required">
								</div>
								<div class="alert alert-danger mt-3" role="alert"
									id="message-password">Tên đăng nhập hoặc mật khẩu không
									chính xác!</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block btn-lg">Đăng
									nhập</button>
							</div>
							<p class="hint-text">
								<a href="forgot">Quên mật khẩu?</a>
							</p>
						</form>
					</div>
					<div class="modal-footer">
						<a href="signup">Tao mới tài khoản</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row mb-4 mt-5">
			<div class="col-lg-8 mx-auto text-center">
				<h1 class="display-6">Thanh toán</h1>
			</div>
		</div>

		<!-- End -->
		<div class="row">
			<div class="col-lg-6 mx-auto">
				<div class="card">
					<div class="card-header">
						<div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
							<!-- Credit card form tabs -->
							<ul role="tablist"
								class="nav bg-light nav-pills rounded nav-fill mb-3">
								<li class="nav-item"><a data-toggle="pill"
									href="#credit-card" class="nav-link active"> <i
										class="fa fa-credit-card mr-2"></i> Đặt hàng
								</a></li>
								<li class="nav-item"><a data-toggle="pill"
									href="#net-banking" class="nav-link"> <i
										class="fa fa-mobile mr-2"></i> Chuyển khoản Online
								</a></li>
							</ul>
						</div>
						<!-- End -->
						<!-- Credit card form content -->
						<div class="tab-content">
							<!-- credit card info-->
							<div id="credit-card" class="tab-pane fade show active pt-3">
								<form role="form" action="checkout" method="POST">
									<div class="form-group">
										<label for="username">
											<h6>Thông tin người nhận</h6>
										</label>
										<div class="input-group">
											<input type="text" placeholder="Tên người nhận"
												name="username" class="form-control mr-2"
												value="<%=kh.getHoTen()%>" required /> <input type="number"
												placeholder="Số điện thoại" name=""
												class="form-control ml-2" required />
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label><span class="hidden-xs">
														<h6>Địa chỉ</h6>
												</span></label>
												<div class="input-group">
													<input type="text" placeholder="Tỉnh/Thành phố" name="city"
														class="form-control" required /> <input type="text"
														placeholder="Quận/Huyện" name="district"
														class="form-control mx-3" required /> <input type="text"
														required class="form-control" placeholder="Phường/Xã" />
												</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										<label for="cardNumber">
											<h6>Địa chỉ đầy đủ</h6>
										</label>
										<div class="input-group">
											<input type="text" name="cardNumber"
												placeholder="Số nhà, tên đường, phường xã"
												class="form-control" required /> <input type="text"
												value="<%=true%>" name="paid" hidden />

										</div>
									</div>
									<div class="card-footer">
										<button class="subscribe btn btn-primary btn-block shadow-sm"
											type="submit">Hoàn tất đặt hàng</button>
									</div>
								</form>
							</div>
							<div id="net-banking" class="tab-pane fade pt-3">
								<div class="form-group">
									<label for="Select Your Bank">
										<h6>Chọn ngân hàng</h6>
									</label> <select class="form-control" id="ccmonth">
										<option value="" selected disabled>Vui lòng chọn ngân
											hàng bạn đang sử dụng</option>
										<option>Vietinbank</option>
										<option>Vietcombank</option>
									</select>
									<div class="alert alert-success mt-3" role="alert">
										<h4 class="alert-heading text-center mb-3">Chuyển khoản
											online</h4>
										<p>
											Quý khách vui lòng chuyển khoản với nội dung ghi rõ <strong>Tên
												& Số Điện Thoại </strong> người nhận hàng <br /> Ví dụ: A THANH
											0988888888
										</p>
										<hr>
										<p class="mb-0">
											VietinBank - Ngân hàng Thương mại cổ phần Công Thương Việt
											Nam <br /> Chủ tài khoản: NGUYEN MANH HUNG <br /> Số tài
											khoản: 0011004366653.
										</p>
									</div>
								</div>

								<!-- End -->
								<!-- End -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>