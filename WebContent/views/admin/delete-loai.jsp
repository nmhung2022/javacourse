<%@page import="model.bean.AdminBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Xoá loại sách</title>

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>
</head>
<body>
	<style>
body {
	overflow: hidden;
}

.form-v7-content {
	height: 400px;
	margin: 30px;
	font-family: 'Open Sans', sans-serif;
	position: relative;
	display: flex;
	justify-content: center;
	display: -webkit-flex;
}

.form-v7-content img {
	object-fit: cover;
	width: 100%;
	height: 100%;
}

.form-v7-content .form-left {
	width: 92.5%;
	margin: 32px 0;
}

.form-v7-content .form-detail {
	padding: 73px 80px 41px;
	position: relative;
	width: 100%;
	background: #fff;
	box-shadow: 0px 8px 20px 0px rgb(0 0 0/ 15%);
	-o-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-ms-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-moz-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-webkit-box-shadow: 0px 8px 20px 0px rgb(0 0 0/ 15%)
}

.form-v7-content .form-detail .form-row {
	width: 100%;
	position: relative;
}

.form-v7-content .form-detail .form-row label {
	color: #666;
	font-weight: 600;
	font-size: 13px;
	margin-bottom: 3px;
}

.form-v7-content .form-detail .input-text {
	margin-bottom: 28px;
}

.form-v7-content .form-detail input {
	width: 100%;
	padding: 5 px 15 px 10 px 15 px;
	border: 2 px solid transparent;
	border-bottom: 2 px solid #e5e5e5;
	appearance: unset;
	-moz-appearance: unset;
	-webkit-appearance: unset;
	-o-appearance: unset;
	-ms-appearance: unset;
	outline: none;
	-moz-outline: none;
	-webkit-outline: none;
	-o-outline: none;
	-ms-outline: none;
	font-family: 'Open Sans', sans-serif;
	font-size: 16px;
	font-weight: 700;
	color: #333;
	box-sizing: border-box;
	-o-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}

.form-row-last {
	margin-top: 35px;
}

.form-v7-content .form-detail .form-row-last input {
	padding: 15px;
}

.form-v7-content .form-detail .register {
	background: #373be3;
	border-radius: 4px;
	-o-border-radius: 4px;
	-ms-border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	width: 180px;
	border: none;
	cursor: pointer;
	color: #fff;
	font-weight: 700;
	font-size: 15px;
}

.form-v7-content .form-detail input {
	width: 100%;
	padding: 5px 15px 10px 15px;
	border: 2px solid transparent;
	border-bottom: 2px solid #e5e5e5;
	appearance: unset;
	-moz-appearance: unset;
	-webkit-appearance: unset;
	-o-appearance: unset;
	-ms-appearance: unset;
	outline: none;
	-moz-outline: none;
	-webkit-outline: none;
	-o-outline: none;
	-ms-outline: none;
	font-family: 'Open Sans', sans-serif;
	font-size: 16px;
	font-weight: 700;
	color: #333;
	box-sizing: border-box;
	-o-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
</style>


	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	%>

	<c:if test="${not empty isSignin}">
		<script>
			$(document).ready(function() {
				$("#loginModal").modal('show');
			});
		</script>
	</c:if>




	<div class="container-fuild vh-50">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link"
						href="dashboard">Quản lý sách <span class="sr-only">unread
								messages</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="manage-loai">Quản
							lý loại sách</a></li>

					<li class="nav-item"><a class="nav-link" href="order-list">Đơn
							hàng</a></li>
				</ul>

				<ul class="navbar-nav ml-auto mr-2">

					<c:choose>
						<c:when test="${not empty sessionScope['auth-admin']}">
							<li class="nav-item"><a class="nav-link" href="profile">Chào
									mừng <c:out
										value="${sessionScope['auth-admin'].getTenDangNhap()
							}" />
							</a></li>
							<li class="nav-item mr-3">
								<form action="signout" method="POST">
									<button type="submit"
										class="nav-link btn btn-sm btn-outline-secondary">Đăng
										xuất</button>
								</form>
							</li>
							<li class="nav-item"><a
								class="btn btn-sm btn-outline-secondary nav-link"
								href="signup-admin">Đăng ký</a></li>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${empty sessionScope['flag_auth']}">
									<c:set scope="request" var="flagAuth" value="0" />
								</c:when>
								<c:otherwise>

									<c:set scope="request" var="flagAuth"
										value="${sessionScope['flag_auth']}" />

								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${not empty flagAuth}">

							<script>
								$(document).ready(
										function() {
											$("#loginModal").modal('show');
											$("#message-password").css(
													"display", "block");
										});
							</script>

						</c:when>
					</c:choose>

				</ul>
			</div>
		</nav>


		<div class="row h-100">
			<div class="col d-flex justify-content-center align-items-center">

				<div class="form-v7-content">
					<form class="form-detail" action="delete-loai" method="get">
						<div class="form-row">
							<label>Mã loại</label> <input type="text" name="ml"
								class="input-text" data-focused="true" disabled
								value="${loaiDelete != null ? loaiDelete.getMaloai() : ''}">
						</div>

						<div class="form-row">
							<label>Tên loại</label> <input type="text" name="tl"
								class="input-text" data-focused="true" disabled
								value="${loaiDelete != null ? loaiDelete.getTenloai() : ''}">
						</div>
						<div class="form-row-last  d-flex justify-content-center">
							<button type="button" class="btn btn-danger ml-3 w-75"
								data-toggle="modal" data-target="#RemoveLoaiModal">Xoá</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="RemoveLoaiModal" tabindex="-1"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Xoá loại sách</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger" role="alert">
						<h4 class="alert-heading">Chú ý</h4>
						<p>Bạn có thật sự muốn xoá? Chọn tiếp tục để xoá</p>
						<hr>
						<p class="mb-0">Chọn đóng để bỏ quả</p>
					</div>
				</div>
				<form action="delete-loai" method="POST">
					<input hidden name="delml" value="${loaiDelete.getMaloai()}">
					<div class="modal-footer">
						<a class="btn btn-secondary" href="dashboard" data-dismiss="modal">Đóng</a>
						<button type="submit" class="btn btn-primary">Tiếp tục</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>