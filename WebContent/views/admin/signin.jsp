<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="model.bean.AdminBean"%>
<%@page import="model.bean.KhachHangBean"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Đăng nhập</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>

</head>
<body>
	<style>
body {
	color: #fff;
	background: #19aa8d;
	font-family: 'Roboto', sans-serif;
	overflow: hidden;
}

.form-control {
	font-size: 15px;
}

.form-control, .form-control:focus, .input-group-text {
	border-color: #e1e1e1;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 440px;
	margin: 0 auto;
	padding: 15px 0;
}

.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #fff;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 25px;
}

.signup-form h2 {
	color: #333;
	font-weight: bold;
	margin-top: 0;
}

.signup-form hr {
	
}

.signup-form .form-group {
	margin-bottom: 15px;
}

.signup-form label {
	font-weight: normal;
	font-size: 15px;
}

.signup-form .form-control {
	min-height: 38px;
	box-shadow: none !important;
}

.signup-form .input-group-addon {
	max-width: 42px;
	text-align: center;
}

.signup-form .btn, .signup-form .btn:active {
	font-size: 16px;
	font-weight: bold;
	background: #19aa8d !important;
	border: none;
	min-width: 140px;
}

.signup-form .btn:hover, .signup-form .btn:focus {
	background: #179b81 !important;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: #19aa8d;
	text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
}

.signup-form .fa {
	font-size: 21px;
}

.signup-form .fa-paper-plane {
	font-size: 18px;
}

.signup-form .fa-check {
	color: #fff;
	left: 17px;
	top: 18px;
	font-size: 7px;
	position: absolute;
}

p {
	margin-bottom: 10px !important;
}

.alert.alert-warning {
	margin-bottom: 0px !important;
}

hr {
	margin: 0.5rem 0;
}

#message-password {
	display: none;
}
</style>

	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	%>


	<c:choose>
		<c:when test="${empty sessionScope['flag_auth']}">

			<c:set scope="request" var="flagAuth" value="0" />
		</c:when>
		<c:otherwise>

			<c:set scope="request" var="flagAuth"
				value="${sessionScope['flag_auth']}" />
		</c:otherwise>
	</c:choose>

	<c:choose>
		<c:when test="${flagAuth eq 1}">
			<script>
				$(document).ready(function() {
					$("#loginModal").modal('show');
					$("#message-password").css("display", "block");
				});
			</script>

		</c:when>
	</c:choose>



	<div class="container-fuild vh-100">
		<div class="row h-100 align-items-center">
			<div class="col">
				<div class="signup-form">
					<form action="signin-admin" method="post" id="signup_form">
						<h2>Đăng nhập Admin</h2>
						<p>Vui lòng đăng nhập với tài khoản đã được cấp sẵn!</p>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <span class="fa fa-user"></span>
									</span>
								</div>
								<input type="text" class="form-control" name="username"
									placeholder="Tên đăng nhập" required="required" id="username">
							</div>
						</div>


						<div class="form-group">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-lock"></i>
									</span>
								</div>
								<input type="password" class="form-control" name="password"
									id="password" placeholder="Mật khẩu" required="required">
								<div class="alert alert-danger mt-3" role="alert"
									id="message-password">Tên đăng nhập hoặc mật khẩu không
									chính xác!</div>
							</div>
						</div>


						<div class="form-group d-flex justify-content-center">
							<button type="submit" class="btn btn-primary btn-lg"
								id="signin_submit">Đăng nhập</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<script>
		
	</script>
</body>
</html>

