<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="model.bean.AdminBean"%>
<%@page import="model.bo.GioHangBo"%>
<%@page import="model.bean.KhachHangBean"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.SachBean"%>

<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>
<title>Quản lý loại</title>
</head>
<body>


	<style>
#message-password {
	display: none;
}

img {
	width: 250px;
	height: 300px;
	object-fit: cover
}

.min {
	min-width: 90px;
}
</style>




	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	%>

	<c:if test="${not empty isSignin}">
		<script>
			$(document).ready(function() {
				$("#loginModal").modal('show');
			});
		</script>
	</c:if>


	<div class="container-fuild">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link"
						href="dashboard">Quản lý sách <span class="sr-only">unread
								messages</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="manage-loai">Quản
							lý loại sách</a></li>

					<li class="nav-item"><a class="nav-link" href="manage-order">Đơn
							hàng</a></li>
				</ul>

				<ul class="navbar-nav ml-auto mr-2">
					<c:choose>
						<c:when test="${not empty sessionScope['auth-admin']}">
							<li class="nav-item"><a class="nav-link" href="profile">Chào
									mừng <c:out
										value="${sessionScope['auth-admin'].getTenDangNhap()
							}" />
							</a></li>
							<li class="nav-item mr-3">
								<form action="signout" method="POST">
									<button type="submit"
										class="nav-link btn btn-sm btn-outline-secondary">Đăng
										xuất</button>
								</form>
							</li>
							<li class="nav-item"><a
								class="btn btn-sm btn-outline-secondary nav-link"
								href="signup-admin">Đăng ký</a></li>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${empty sessionScope['flag_auth']}">

									<c:set scope="request" var="flagAuth" value="0" />
								</c:when>
								<c:otherwise>

									<c:set scope="request" var="flagAuth"
										value="${sessionScope['flag_auth']}" />

								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${not empty flagAuth}">

							<script>
								$(document).ready(
										function() {
											$("#loginModal").modal('show');
											$("#message-password").css(
													"display", "block");
										});
							</script>

						</c:when>
					</c:choose>

				</ul>
			</div>
		</nav>

		<%
		if (request.getAttribute("newAdmin") != null) {
		%>
		<div class="alert alert-warning alert-dismissible fade show"
			role="alert">
			<strong>Đăng ký thành công!</strong>
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<%
		}
		%>

		<table class="table">
			<tr>
				<td valign="top" width="100">
					<form action="seachloai" method="GET">
						<div class="form-group">
							<label for="exampleInputPassword1"> <c:choose>
									<c:when test="${not empty timkiemloai}">
										<p>
											Kết quả tìm cho
											<c:out value="${timkiemloai}"></c:out>
										</p>
									</c:when>
									<c:otherwise>
										<p>Tìm kiếm loại</p>
									</c:otherwise>
								</c:choose>

							</label> <input type="text" class="form-control" placeholder="Tìm kiếm"
								name="txtLoai">
						</div>
						<div class="form-group mt-4">
							<button class="btn btn-outline-success my-2 my-sm-0"
								type="submit">Tìm kiếm</button>

						</div>
					</form>
				</td>
				<td width="700">
					<table class="table">
						<tr>
							<div class="row">
								<div class="col-9">
									<h4>
										Có
										<c:out value="${dsloai.size()}" />
										loại sách
									</h4>
								</div>
								<div class="col-e">
									<a class="btn btn-outline-success"
										href="add-or-edit-loai?add=<%=true%>" role="button">Thêm
										loại sách</a>
								</div>
							</div>
							<td>
								<div class="container">
									<c:if test="${deleteSuccess != null }">
										<div class="col">
											<div class="alert alert-success alert-dismissible fade show"
												role="alert">
												Loại sách có mã <strong><c:out
														value="${deleteSuccess}"></c:out></strong> đã được xoá
												<p class="mb-0">
													Bấm vào <strong>đây</strong> để khôi phục.
												</p>
												<button type="button" class="close" data-dismiss="alert"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
										</div>
									</c:if>

									<c:if test="${loaiAddNew != null }">
										<div class="col">
											<div class="alert alert-success alert-dismissible fade show"
												role="alert">
												<div class="alert alert-success" role="alert">

													<h4 class="alert-heading">Thêm loại sách thành công!</h4>
													<p>
														Loại sách có tên loại<strong> <c:out
																value="${loaiAddNew.getTenloai()}"></c:out></strong> vừa mới được
														thêm vào.
													</p>
												</div>
												<button type="button" class="close" data-dismiss="alert"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>

										</div>
									</c:if>

									<c:if test="${loaiEditNew != null }">
										<div class="col">
											<div class="alert alert-success alert-dismissible fade show"
												role="alert">
												<div class="alert alert-success" role="alert">

													<h4 class="alert-heading">Cập nhật thành công!</h4>
													<p>
														Loại sách có tên<strong> <c:out
																value="${loaiEditNew.getTenloai()}"></c:out></strong> vừa mới
														được cập nhật.
													</p>
												</div>
												<button type="button" class="close" data-dismiss="alert"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>

										</div>
									</c:if>


									<div class="row">


										<c:forEach items="${dsloai}" var="l">
											<div class="col-4 my-3">
												<div class="col">
													<p>Mã loại: ${l.getMaloai()}</p>
													<p>Tên Loại: ${l.getTenloai()}</p>
												</div>
												<a class="btn btn-warning min"
													href="add-or-edit-loai?edit=<%=false%>&ml=${l.getMaloai()}&tl=${l.getTenloai()}">Chỉnh
													sửa </a> <a class="btn btn-danger ml-3 min"
													href="delete-loai?ml=${l.getMaloai()}&tl=${l.getTenloai()}">Xoá</a>
											</div>
										</c:forEach>
									</div>
								</div>

							</td>

						</tr>
					</table>

				</td>
		</table>
	</div>



</body>
</html>