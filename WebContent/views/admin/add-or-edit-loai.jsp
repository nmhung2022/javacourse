<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.SachBean"%>
<%@page import="model.bean.AdminBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:choose>
	<c:when test="${addOrEdit eq 'true'}">
		<title>Thêm loại sách</title>
	</c:when>
	<c:otherwise>
		<title>Cập nhật loại sách</title>
	</c:otherwise>
</c:choose>



<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>
</head>
<body>
	<style>
body {
	overflow: hidden;
}

.form-v7-content {
	height: 350px;
	margin: 25px;
	font-family: 'Open Sans', sans-serif;
	position: relative;
	display: flex;
	justify-content: center;
	display: -webkit-flex;
	width: 63%;
}

.form-v7-content img {
	object-fit: cover;
	width: 100%;
	height: 100%;
}

.form-v7-content .form-left {
	width: 90%;
	margin: 32px 0;
}

.form-v7-content .form-detail {
	width: inherit;
	padding-top: 20px;
	padding-right: 80px;
	padding-left: 80px;
	position: relative;
	background: #fff;
	box-shadow: 0px 8px 20px 0px rgb(0 0 0/ 15%);
	-o-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-ms-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-moz-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-webkit-box-shadow: 0px 8px 20px 0px rgb(0 0 0/ 15%);
	padding-top: 20px;
}

.form-v7-content .form-detail .form-row {
	width: 100%;
	position: relative;
}

.form-v7-content .form-detail .form-row label {
	color: #666;
	font-weight: 600;
	font-size: 13px;
	margin-bottom: 3px;
}

.form-v7-content .form-detail .input-text {
	margin-bottom: 15px;
}

.form-v7-content .form-detail input {
	width: 100%;
	padding: 5 px 15 px 10 px 15 px;
	border: 2 px solid transparent;
	border-bottom: 2 px solid #e5e5e5;
	appearance: unset;
	-moz-appearance: unset;
	-webkit-appearance: unset;
	-o-appearance: unset;
	-ms-appearance: unset;
	outline: none;
	-moz-outline: none;
	-webkit-outline: none;
	-o-outline: none;
	-ms-outline: none;
	font-family: 'Open Sans', sans-serif;
	font-size: 16px;
	font-weight: 700;
	color: #333;
	box-sizing: border-box;
	-o-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}

.form-row-last {
	margin-top: 20px;
}

.form-v7-content .form-detail .form-row-last input {
	padding: 15px;
}

.form-v7-content .form-detail .register {
	background: #373be3;
	border-radius: 4px;
	-o-border-radius: 4px;
	-ms-border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	width: 180px;
	border: none;
	cursor: pointer;
	color: #fff;
	font-weight: 700;
	font-size: 15px;
}

.form-v7-content .form-detail input {
	width: 100%;
	padding: 5px 15px 10px 15px;
	border: 2px solid transparent;
	border-bottom: 2px solid #e5e5e5;
	appearance: unset;
	-moz-appearance: unset;
	-webkit-appearance: unset;
	-o-appearance: unset;
	-ms-appearance: unset;
	outline: none;
	-moz-outline: none;
	-webkit-outline: none;
	-o-outline: none;
	-ms-outline: none;
	font-family: 'Open Sans', sans-serif;
	font-size: 16px;
	font-weight: 700;
	color: #333;
	box-sizing: border-box;
	-o-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
</style>


	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	%>


	<c:if test="${not empty isSignin}">
		<script>
			$(document).ready(function() {
				$("#loginModal").modal('show');
			});
		</script>
	</c:if>




	<div class="container-fuild vh-100">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link"
						href="dashboard">Quản lý sách <span class="sr-only">unread
								messages</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="manage-loai">Quản
							lý loại sách</a></li>

					<li class="nav-item"><a class="nav-link" href="order-list">Đơn
							hàng</a></li>
				</ul>

				<ul class="navbar-nav ml-auto mr-2">

					<c:choose>
						<c:when test="${not empty sessionScope['auth-admin']}">
							<li class="nav-item"><a class="nav-link" href="profile">Chào
									mừng <c:out
										value="${sessionScope['auth-admin'].getTenDangNhap()
							}" />
							</a></li>
							<li class="nav-item mr-3">
								<form action="signout" method="POST">
									<button type="submit"
										class="nav-link btn btn-sm btn-outline-secondary">Đăng
										xuất</button>
								</form>
							</li>
							<li class="nav-item"><a
								class="btn btn-sm btn-outline-secondary nav-link"
								href="signup-admin">Đăng ký</a></li>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${empty sessionScope['flag_auth']}">
									<c:set scope="request" var="flagAuth" value="0" />
								</c:when>
								<c:otherwise>

									<c:set scope="request" var="flagAuth"
										value="${sessionScope['flag_auth']}" />

								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${not empty flagAuth}">

							<script>
								$(document).ready(
										function() {
											$("#loginModal").modal('show');
											$("#message-password").css(
													"display", "block");
										});
							</script>

						</c:when>
					</c:choose>

				</ul>
			</div>
		</nav>


		<div class="row h-100">
			<div class="col d-flex justify-content-center align-items-center">

				<div class="form-v7-content">

					<form class="form-detail" action="add-or-edit-loai" method="get">
						<div class="form-row">
							<label>Mã loại</label> <input type="text" name="ml-old" hidden
								value="${existsMaLoai != null ? existsMaLoai.getMaloai() : ''}">

							<input type="text" name="ml" class="input-text"
								data-focused="true" required
								value="${existsMaLoai != null ? existsMaLoai.getMaloai() : ''}">
							<c:if test="${existsMaLoai != null }">
								<div class="alert alert-warning" role="alert">
									Mã loại sách <strong><c:out
											value="${existsMaLoai.getMaloai()}"></c:out></strong> đã tồn tại, vui
									lòng thử lại với tên khác
								</div>
							</c:if>
						</div>


						<div class="form-row">
							<label>Tên loại</label> <input type="text" name="tl"
								class="input-text" data-focused="true" required
								value="${existsMaLoai != null ? existsMaLoai.getTenloai() : ''}">
						</div>
						<div class="form-row-last  d-flex justify-content-center">
							<c:choose>
								<c:when test="${addOrEdit eq 'true'}">
									<input type="submit" class="register" name="add"
										value="Thêm mới">
								</c:when>
								<c:otherwise>
									<input type="submit" class="register" name="update"
										value="Cập nhật">
								</c:otherwise>
							</c:choose>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>