<%@page import="java.text.SimpleDateFormat"%>
<%@page import="model.bean.LichSuMuaHangBean"%>
<%@page import="model.bo.LichSuMuaHangBo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="model.bean.AdminBean"%>
<%@page import="model.bo.GioHangBo"%>
<%@page import="model.bean.KhachHangBean"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.SachBean"%>

<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
	integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
	integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
	crossorigin="anonymous"></script>
<title>Quản lý hoá đơn</title>
</head>
<body>


	<style>
#message-password {
	display: none;
}

img {
	width: 250px;
	height: 300px;
	object-fit: cover
}

.min {
	min-width: 90px;
}

.min-width {
	min-width: 95px;
}
</style>




	<%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");

	LichSuMuaHangBo listOrderHistory = (LichSuMuaHangBo) request.getAttribute("orderHistory");

	int size = 0;
	if (session.getAttribute("giohang") != null) {
		GioHangBo ghbo = new GioHangBo();

		ghbo = (GioHangBo) session.getAttribute("giohang");

		size = ghbo.ds.size();
	}
	%>

	<c:if test="${not empty isSignin}">
		<script>
			$(document).ready(function() {
				$("#loginModal").modal('show');
			});
		</script>
	</c:if>


	<div class="container-fuild">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link"
						href="dashboard">Quản lý sách <span class="sr-only">unread
								messages</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="manage-loai">Quản
							lý loại sách</a></li>

					<li class="nav-item"><a class="nav-link" href="order-list">Đơn
							hàng</a></li>
				</ul>

				<ul class="navbar-nav ml-auto mr-2">
					<c:choose>
						<c:when test="${not empty sessionScope['auth-admin']}">
							<li class="nav-item"><a class="nav-link" href="profile">Chào
									mừng <c:out
										value="${sessionScope['auth-admin'].getTenDangNhap()
							}" />
							</a></li>
							<li class="nav-item mr-3">
								<form action="signout" method="POST">
									<button type="submit"
										class="nav-link btn btn-sm btn-outline-secondary">Đăng
										xuất</button>
								</form>
							</li>
							<li class="nav-item"><a
								class="btn btn-sm btn-outline-secondary nav-link"
								href="signup-admin">Đăng ký</a></li>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${empty sessionScope['flag_auth']}">

									<c:set scope="request" var="flagAuth" value="0" />
								</c:when>
								<c:otherwise>

									<c:set scope="request" var="flagAuth"
										value="${sessionScope['flag_auth']}" />

								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${not empty flagAuth}">

							<script>
								$(document).ready(
										function() {
											$("#loginModal").modal('show');
											$("#message-password").css(
													"display", "block");
										});
							</script>

						</c:when>
					</c:choose>

				</ul>
			</div>
		</nav>

		<div class="row my-5 d-flex justify-content-center">
			<div class="col col-8">
				<div class="card card-1">
					<div class="card-header bg-white">
						<div
							class="media flex-sm-row flex-column-reverse justify-content-between ">
							<div class="col my-auto">
								<h4 class="mb-0">Danh sách mua hàng</h4>
							</div>
						</div>
						<c:if test="${deleteSuccess != null }">
							<div class="col">
								<div
									class="alert alert-success alert-dismissible fade show mt-2"
									role="alert">
									Hoá đơn có mã <strong><c:out value="${deleteSuccess}"></c:out></strong>
									đã được xoá
									<p class="mb-0">
										Bấm vào <strong>đây</strong> để khôi phục.
									</p>
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
						</c:if>
					</div>
					<div class="card-body">
						<div class="row justify-content-between mb-3">

							<div class="col-auto ">
								<strong>Tổng số đơn hàng <%=listOrderHistory.ds.size()%>
								</strong>
							</div>
						</div>

						<div class="row card-wrap">
							<div class="col">
								<table class="table table table-bordered">
									<thead>
										<tr>
											<th scope="col">Đơn hàng</th>
											<th scope="col">Thời gian mua</th>
											<th scope="col">Sản phẩm</th>
											<th scope="col">Tổng tiền</th>
											<th scope="col">Trạng thái</th>
											<th scope="col">Tác vụ</th>
										</tr>
									</thead>


									<tbody>
										<%
										for (LichSuMuaHangBean ls : listOrderHistory.ds) {
										    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss dd/MM/yyyy ");
										%>
										<tr>
											<td><%=ls.getMaHoaDon()%></td>
											<td><%=sdf.format(ls.getThoiGianMua())%></td>
											<td><%=ls.getTenSach()%></td>
											<td><%=ls.getTongTien()%></td>
											<%
											if (ls.getTrangThai() == 0) {
											%>
											<td>Chưa thanh toán</td>
											<%
											} else if (ls.getTrangThai() == 1) {
											%>
											<td>Đang giao hàng</td>
											<%
											} else if (ls.getTrangThai() == 2) {
											%>
											<td>Đã thanh toán</td>
											<%
											}
											%>
											<%
											if (ls.getTrangThai() == 0) {
											%>
											<td>
												<div class="col d-flex align-items-center">
													<a class="btn btn-danger min-width"
														href="delete-order?mhd=<%=ls.getMaHoaDon()%>">Xoá</a> <a
														class="btn btn-success min-width ml-2"
														href="update-order?mhd=<%=ls.getMaHoaDon()%>&tt=<%=ls.getTrangThai()%>">Xác
														nhận</a>

												</div>
											</td>
											<%
											} else if (ls.getTrangThai() == 1) {
											%>
											<td>
												<div class="col d-flex align-items-center">
													<a class="btn btn-success min-width ml-2"
														href="update-order?mhd=<%=ls.getMaHoaDon()%>&tt=<%=ls.getTrangThai()%>">Đã
														nhận tiền</a>

												</div>
											</td>
											<%
											} else if (ls.getTrangThai() == 2) {
											%>
											<td>
												<div class="col d-flex align-items-center">
													<a class="btn btn-success min-width ml-2"
														href="delete-order?mhd=<%=ls.getMaHoaDon()%>">Xoá</a>

												</div>
											</td>
											<%
											}
											%>
										</tr>
										<%
										}
										%>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>



</body>
</html>