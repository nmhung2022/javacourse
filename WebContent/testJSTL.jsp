<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h3>
		accountBalance=
		<c:out value="${tb}" />
	</h3>
	<c:forEach items="${dssach}" var="s">
		<c:choose>
			<c:when test="${not empty s.getAnh()}">
				<img alt="" src="${s.getAnh()}">
			</c:when>
			<c:otherwise>
				<img alt="" src="https://picsum.photos/200/300">
			</c:otherwise>
		</c:choose>
	</c:forEach>
</body>
</html>